using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Notegate
{
    public enum NotepadOption : byte { Create, Open }

    public class NotepadManager
    {
        public static string DirectoryPath { get; } = Path.Combine(Environment.CurrentDirectory, "notepads");

        [XmlAttribute]
        public string Salt => _cryptoUtilities.Salt;

        [XmlAttribute]
        public string HashedSaltedPassword { get; set; }

        public List<Note> Notes { get; } = new();

        private CryptographicUtilities _cryptoUtilities;

        private string _filename;

        private string XmlNotepadElement = "notepad";

        private string XmlNoteElement = "note";

        private const string XmlTitleAttribute = "title";

        private const string XmlContentAttribute = "content";

        private const string XmlDateAttribute = "date";

        private const string XmlSaltAttribute = "salt";

        private const string XmlPasswordAttribute = "password";

        /// <summary>
        /// Creates a new instance of NotepadManager.
        /// </summary>
        /// <param name="filename">The name of a notepad.</param>
        /// <param name="password">The password used to encrypt/decrypt the contents of a notepad.</param>
        /// <param name="option">The option to either load or create a new notepad.</param>
        public NotepadManager(string filename, string password, NotepadOption option)
        {
            _filename = $"{filename}.xml";
            Directory.CreateDirectory(DirectoryPath);

            switch (option)
            {
                case NotepadOption.Create:
                    _cryptoUtilities = new CryptographicUtilities(password);
                    HashedSaltedPassword = _cryptoUtilities.HashedSaltedPassword;
                    SaveNotepad();
                    break;

                case NotepadOption.Open:
                    LoadNotepad(password);
                    break;
            }
        }

        private void LoadNotepad(string password)
        {
            using var xmlReader = XmlReader.Create(Path.Combine(DirectoryPath, _filename), new XmlReaderSettings { IgnoreWhitespace = true });

            xmlReader.ReadToFollowing(XmlNotepadElement);

            xmlReader.MoveToFirstAttribute();
            var salt = xmlReader.Value;

            xmlReader.MoveToNextAttribute();
            HashedSaltedPassword = xmlReader.Value;

            _cryptoUtilities = new CryptographicUtilities(password, salt);

            string title = null, content = null, date = null;
            while (xmlReader.Read())
            {
                if (xmlReader.IsStartElement())
                {
                    switch (xmlReader.Name.ToString())
                    {
                        case XmlTitleAttribute:
                            title = _cryptoUtilities.DecryptString(xmlReader.ReadString());
                            break;

                        case XmlContentAttribute:
                            content = _cryptoUtilities.DecryptString(xmlReader.ReadString());
                            break;

                        case XmlDateAttribute:
                            date = xmlReader.ReadString();
                            Notes.Add(new Note(title, content, System.Convert.ToDateTime(date)));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Commit pending notepad changes to the file.
        /// </summary>
        public void SaveNotepad()
        {
            var xmlWriter = XmlWriter.Create(Path.Combine(DirectoryPath, _filename), new XmlWriterSettings { Indent = true });

            xmlWriter.WriteStartDocument();

            xmlWriter.WriteStartElement(XmlNotepadElement);
            xmlWriter.WriteAttributeString(XmlSaltAttribute, Salt);
            xmlWriter.WriteAttributeString(XmlPasswordAttribute, HashedSaltedPassword);

            foreach (var note in Notes)
            {
                xmlWriter.WriteStartElement(XmlNoteElement);
                xmlWriter.WriteElementString(XmlTitleAttribute, _cryptoUtilities.EncryptString(note.Title));
                xmlWriter.WriteElementString(XmlContentAttribute, _cryptoUtilities.EncryptString(note.Content));
                xmlWriter.WriteElementString(XmlDateAttribute, note.Date.ToString());
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }

        /// <summary>
        /// Verify whether given password is correct for a given notepad.
        /// </summary>
        /// <param name="filename">The name of a notepad without any extensions.<</param>
        /// <param name="password">The password used to encrypt/decrypt the contents of a notepad.</param>
        /// <returns></returns>
        public static bool CheckPassword(string filename, string password)
        {
            if (NotepadExists(filename))
            {
                // Salt and hash recieved password to compare it to the original
                // If they are identical, the contents will be decrypted correctly
                try
                {
                    var tempNotepad = new NotepadManager(filename, password, NotepadOption.Open);

                    if (tempNotepad.HashedSaltedPassword.Equals(tempNotepad.HashedSaltedPassword))
                    {
                        return true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return false;
        }

        /// <summary>
        /// Check whether the note with given title exists within the currently opened notepad.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public bool NoteExists(string title) => Notes.Any(n => n.Title.Equals(title));

        /// <summary>
        /// Check whether the notepead with given name exists.
        /// </summary>
        /// <param name="filename">Name of the notepad without any extensions.</param>
        /// <returns></returns>
        public static bool NotepadExists(string filename) => File.Exists(Path.Combine(DirectoryPath, $"{filename}.xml"));
    }
}
