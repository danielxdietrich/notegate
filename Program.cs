﻿namespace Notegate
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            new NotepadInterface().Run();
        }
    }
}
