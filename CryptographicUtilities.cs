using System.IO;
using System.Security.Cryptography;

using static System.Text.Encoding;
using static System.Convert;

namespace Notegate
{
    public class CryptographicUtilities
    {
        public string Password { get; }
        public string HashedSaltedPassword { get; }
        public string Salt { get; }
        public int Iterations { get; }

        private readonly Rfc2898DeriveBytes _pbkdf2;
        private readonly Aes _aes;

        /// <summary>
        /// Initializes a new instance of `CryptographicUtilities` class which allows for encryption and decryption of string variables.
        /// To decrypt a certain string variable, the password and salt must be the same as during the encryption.
        /// </summary>
        /// <param name="password">Password used to encrypt/decrypt the string variables.</param>
        /// <param name="salt">Series of random characters added to the password to increase security.</param>
        /// <param name="iterations"></param>
        public CryptographicUtilities(string password, string salt = null, int iterations = 1000)
        {
            Password = password;
            Iterations = iterations;
            Salt = salt ?? GenerateSalt();
            HashedSaltedPassword = HashAndSaltPassword();

            _pbkdf2 = new Rfc2898DeriveBytes(Password, Unicode.GetBytes(Salt), Iterations);
            _aes = Aes.Create();

            _aes.Key = _pbkdf2.GetBytes(32);
            _aes.IV = _pbkdf2.GetBytes(16);
        }

        /// <summary>
        /// Decrypts a given string variable with password and salt of this instance.
        /// </summary>
        /// <param name="unencryptedText"></param>
        /// <returns>Decrypted string variable.</returns>
        public string DecryptString(string encryptedText)
        {
            byte[] unencryptedBytes;
            byte[] encryptedBytes = FromBase64String(encryptedText);

            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, _aes.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(encryptedBytes);
                }
                unencryptedBytes = ms.ToArray();
            }

            return Unicode.GetString(unencryptedBytes);
        }

        /// <summary>
        /// Encrypts a given string variable with password and salt of this instance.
        /// </summary>
        /// <param name="unencryptedText"></param>
        /// <returns>Encrypted Base64String string variable.</returns>
        public string EncryptString(string unencryptedText)
        {
            byte[] encryptedBytes;
            byte[] unencryptedBytes = Unicode.GetBytes(unencryptedText);

            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, _aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(unencryptedBytes);
                }
                encryptedBytes = ms.ToArray();
            }

            return ToBase64String(encryptedBytes);
        }

        private string GenerateSalt()
        {
            var saltBytes = new byte[16];
            var rng = RandomNumberGenerator.Create();

            rng.GetBytes(saltBytes);
            return ToBase64String(saltBytes);
        }


        private string HashAndSaltPassword()
        {
            var sha = SHA256.Create();

            var saltedPassword = Password + Salt;
            var saltedPasswordBytes = Unicode.GetBytes(saltedPassword);
            var saltedPasswordHash = sha.ComputeHash(saltedPasswordBytes);

            return ToBase64String(saltedPasswordHash);
        }
    }
}
