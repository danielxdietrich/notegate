using System.IO;
using System.Linq;
using System.Text;

using static System.Console;

namespace Notegate
{
    public class NotepadInterface
    {
        private NotepadManager _notepad;

        public void Run()
        {
            string command;

            while (true)
            {
                WriteLine("> Available commands:");
                WriteLine("create - create new notepad");
                WriteLine("open - open existing notepad");
                WriteLine("delete - delete existing notepad");
                WriteLine("list - list all available notepads");
                WriteLine("exit - exit application");
                command = ReadSeperatedLine();

                switch (command)
                {
                    case "create":
                        CreateNotepadCommand();
                        break;

                    case "open":
                        OpenNotepadCommand();
                        break;

                    case "delete":
                        DeleteNotepadCommand();
                        break;

                    case "list":
                        ListNotepadsCommand();
                        break;

                    case "exit":
                        System.Environment.Exit(1);
                        break;

                    default:
                        WriteLine($"[{command}] is not a command.");
                        break;
                }
                DrawSeperator();
            }
        }

        private void CreateNotepadCommand()
        {
            string filename, password;

            Write("> Enter name for a new notepad: ");
            filename = ReadSeperatedLine();
            if (string.IsNullOrWhiteSpace(filename))
            {
                WriteLine("> Name can't be empty.");
            }
            else if (NotepadManager.NotepadExists(filename))
            {
                WriteLine("> Notepad with that name already exists.");
            }
            else
            {

                Write("> Enter password for a new notepad: ");
                if (!string.IsNullOrWhiteSpace(password = ReadSeperatedLine()))
                {
                    _notepad = new NotepadManager(filename, password, NotepadOption.Create);
                    ListNotepadCommands();
                }
                else
                {
                    WriteLine("> Password can't be empty.");
                }

            }
        }

        private void OpenNotepadCommand()
        {
            string filename, password;

            Write("> Enter the name of the notepad you wish to open: ");
            filename = ReadSeperatedLine();
            if (NotepadManager.NotepadExists(filename))
            {
                Write("> Enter the password: ");
                password = ReadSeperatedLine();
                if (NotepadManager.CheckPassword(filename, password))
                {
                    WriteLine("> Notepad opened succesfully.");
                    DrawSeperator();
                    _notepad = new NotepadManager(filename, password, NotepadOption.Open);
                    ListNotepadCommands();
                }
                else
                {
                    WriteLine("> Password is invalid.");
                }
            }
            else
            {
                WriteLine("> Notepad with that name doesn't exist.");
            }
        }


        private void DeleteNotepadCommand()
        {
            Write("> Enter notepad's name: ");
            var filename = ReadSeperatedLine();

            if (NotepadManager.NotepadExists(filename))
            {
                File.Delete(Path.Combine(NotepadManager.DirectoryPath, $"{filename}.xml"));
                WriteLine("> Notepad deleted succesfully.");
            }
            else
            {
                WriteLine("> Notepad with that name doesn't exist.");
            }
        }

        private void ListNotepadsCommand()
        {
            WriteLine("> Available notepads:");

            Directory.CreateDirectory(NotepadManager.DirectoryPath);
            var notepads = Directory.GetFiles(NotepadManager.DirectoryPath).Where(f => f.EndsWith(".xml")).ToArray();

            if (notepads.Length > 0)
            {
                foreach (var notepad in notepads)
                {
                    WriteLine($"- {Path.GetFileNameWithoutExtension(notepad)}");
                }
            }
            else
            {
                WriteLine("> No notepads found.");
            }
        }

        private void ListNotepadCommands()
        {
            string command;

            while (true)
            {
                WriteLine("> Available notepad commands");
                WriteLine("create - create a new note");
                WriteLine("open - open a note");
                WriteLine("delete - delete a note");
                WriteLine("list - list all notes");
                WriteLine("exit - exit application");
                command = ReadSeperatedLine();

                switch (command)
                {
                    case "create":
                        CreateNoteCommand();
                        break;

                    case "open":
                        OpenNoteCommand();
                        break;

                    case "delete":
                        DeleteNoteCommand();
                        break;

                    case "list":
                        ListNotesCommand();
                        break;

                    case "exit":
                        System.Environment.Exit(1);
                        break;

                    default:
                        WriteLine($"[{command}] is not a command.");
                        break;
                }
                DrawSeperator();
            }
        }

        private void CreateNoteCommand()
        {
            string title, content;

            while (true)
            {
                Write("Enter title: ");
                title = ReadSeperatedLine();

                if (string.IsNullOrWhiteSpace(title))
                {
                    WriteLine("> Title can't be empty.");
                }
                else if (_notepad.Notes.Any(n => n.Title.Equals(title)))
                {
                    WriteLine("> Note with that name already exists");
                }
                else
                {
                    break;
                }
            }

            while (true)
            {
                Write("Enter content: ");
                content = ReadSeperatedLine(false);

                if (string.IsNullOrWhiteSpace(content))
                {
                    WriteLine("> Content can't be empty.");
                }
                else
                {
                    break;
                }
            }

            _notepad.Notes.Add(new Note(title, content));
            _notepad.SaveNotepad();
        }

        private void OpenNoteCommand()
        {
            string title;

            Write("Enter the title of a note you wish to read: ");
            title = ReadSeperatedLine();
            if (!string.IsNullOrWhiteSpace(title))
            {
                if (_notepad.NoteExists(title))
                {
                    var content = _notepad.Notes.First(n => n.Title.Equals(title)).Content;
                    var date = _notepad.Notes.First(n => n.Title.Equals(title)).Date;

                    WriteLine($"Title: {title}");
                    WriteLine($"Date: {date.ToShortDateString()}");
                    WriteLine($"Contents: {content}");
                }
                else
                {
                    WriteLine("> Note not found.");
                }
            }
            else
            {
                WriteLine("> Title can't be empty.");
            }
        }

        private void DeleteNoteCommand()
        {
            string title;

            Write("Enter the title of a note you wish to delete: ");
            title = title = ReadSeperatedLine();
            if (!string.IsNullOrWhiteSpace(title))
            {
                if (_notepad.NoteExists(title))
                {
                    _notepad.Notes.RemoveAll(note => note.Title.Equals(title));
                    _notepad.SaveNotepad();
                }
                else
                {
                    WriteLine("> Note not found.");
                }
            }
            else
            {
                WriteLine("> Title can't be empty.");
            }
        }

        private void ListNotesCommand()
        {
            WriteLine("> Available notes:");
            if (_notepad.Notes.Count > 0)
            {
                foreach (var note in _notepad.Notes)
                {
                    WriteLine($"- {note.Title}");
                }
            }
            else
            {
                WriteLine("> Notes not found.");
            }
        }

        private void DrawSeperator(int length = 40)
        {
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                stringBuilder.Append('-');
            }

            WriteLine(stringBuilder.ToString());
        }

        private string ReadSeperatedLine(bool drawLine = true)
        {
            var input = ReadLine();

            if (drawLine)
            {
                DrawSeperator();
            }

            return input;
        }
    }
}